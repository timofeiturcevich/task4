package com.epam.esm.services;

import com.epam.esm.entities.*;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.security.oauth2.server.authorization.settings.OAuth2TokenFormat;
import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;

import java.time.Duration;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class Converter {

    public static Client from(RegisteredClient registeredClient){
        Client client = new Client();
        client.setClientId(registeredClient.getClientId());
        client.setSecret(registeredClient.getClientSecret());
        client.setAuthenticationMethods(
                registeredClient.getClientAuthenticationMethods()
                .stream().map(
                         i->from(i,client)
                )
                .collect(Collectors.toList())
        );
        client.setGrantTypes(
                registeredClient.getAuthorizationGrantTypes()
                .stream().map(
                        i->from(i,client)
                )
                .collect(Collectors.toList())
        );
        client.setRedirectURLS(
                registeredClient.getRedirectUris()
                .stream().map(
                        i->urlFrom(i,client)
                )
                .collect(Collectors.toList())
        );
        client.setScopes(
                registeredClient.getScopes()
                .stream().map(
                        i->scopeFrom(i,client)
                )
                .collect(Collectors.toList())
        );
        client.setTokenSettings(from(registeredClient.getTokenSettings(),client));
        return client;
    }

    public static RegisteredClient from(Client client){
        return RegisteredClient.withId(String.valueOf(client.getId()))
                .clientId(client.getClientId())
                .clientSecret(client.getSecret())
                .clientAuthenticationMethods(clientAuthenticationMethods(client.getAuthenticationMethods()))
                .authorizationGrantTypes(authorizationGrantTypes(client.getGrantTypes()))
                .scopes(scopes(client.getScopes()))
                .redirectUris(uris(client.getRedirectURLS()))
                .tokenSettings(
                        TokenSettings.builder()
                                .accessTokenTimeToLive(Duration.ofHours(client.getTokenSettings().getAccessTokenTTL()))
                                .accessTokenFormat(new OAuth2TokenFormat(client.getTokenSettings().getType()))
                                .refreshTokenTimeToLive(Duration.ofHours(client.getTokenSettings().getAccessTokenTTL()* 2L))
                                .reuseRefreshTokens(true)
                                .build()
                )
                .build();
    }

    private static Consumer<Set<AuthorizationGrantType>> authorizationGrantTypes(List<GrantType> grantTypes){
        return i->{
            for (GrantType tempt : grantTypes){
                i.add(new AuthorizationGrantType(tempt.getType()));
            }
        };
    }

    private static Consumer<Set<ClientAuthenticationMethod>> clientAuthenticationMethods(List<AuthenticationMethod> authenticationMethods){
        return i->{
            for(AuthenticationMethod tempt : authenticationMethods){
                i.add(new ClientAuthenticationMethod(tempt.getMethod()));
            }
        };
    }

    private static Consumer<Set<String>> scopes(List<Scope> scopes){
        return i->{
            for(Scope tempt : scopes){
                i.add(tempt.getScope());
            }
        };
    }

    private static Consumer<Set<String>> uris(List<RedirectURL> redirectURLS){
        return i->{
            for(RedirectURL tempt : redirectURLS){
                i.add(tempt.getLink());
            }
        };
    }
    private static AuthenticationMethod from(ClientAuthenticationMethod clientAuthenticationMethod, Client client){
        return AuthenticationMethod.builder()
                .method(clientAuthenticationMethod.getValue())
                .client(client)
                .build();
    }

    private static GrantType from(AuthorizationGrantType authorizationGrantType, Client client){
        return GrantType.builder()
                .type(authorizationGrantType.getValue())
                .client(client)
                .build();
    }

    private static RedirectURL urlFrom(String link, Client client){
        return RedirectURL.builder()
                .link(link)
                .client(client)
                .build();
    }

    private static Scope scopeFrom(String scope, Client client){
        return Scope.builder()
                .scope(scope)
                .client(client)
                .build();
    }

    private static ClientTokenSettings from(TokenSettings tokenSettings, Client client){
        return ClientTokenSettings.builder()
                .accessTokenTTL((int) tokenSettings.getAccessTokenTimeToLive().toHours())
                .type(tokenSettings.getAccessTokenFormat().getValue())
                .client(client)
                .build();
    }
}
