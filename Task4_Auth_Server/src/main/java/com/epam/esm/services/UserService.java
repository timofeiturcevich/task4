package com.epam.esm.services;

import com.epam.esm.entities.Authority;
import com.epam.esm.entities.User;
import com.epam.esm.model.SecurityUser;
import com.epam.esm.repositories.AuthorityRepository;
import com.epam.esm.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthorityRepository authorityRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return new SecurityUser(
                userRepository.findUserByEmail(username)
                        .orElseThrow()
        );
    }

    @Transactional
    public Long createOidcUser(Map<Object, Object> fields){
        Authority authority = authorityRepository.findAllByName("USER")
                .orElse(new Authority("USER"));
        User user = User.builder()
                .email(fields.get("email").toString())
                .password(passwordEncoder.encode(fields.get("password").toString()))
                .lastName(fields.get("lastName").toString())
                .firstName(fields.get("firstName").toString())
                .build();
        user.addAuthority(authority);

        return userRepository.save(user).getId();
    }

    @Transactional
    public Long updateUser(Map<Object,Object> fields){
        User user = userRepository.findUserByEmail(fields.get("username").toString())
                .orElseThrow();

        user.setPassword(passwordEncoder.encode(fields.get("password").toString()));
        return user.getId();
    }

    @Transactional
    public Map<String, Object> createOidcUser(String username) {
        SecurityUser tempt = new SecurityUser(
                userRepository.findUserByEmail(username)
                        .orElseThrow()
        );
        return OidcUserInfo.builder()
                .subject(username)
                .givenName(tempt.getUser().getFirstName())
                .familyName(tempt.getUser().getLastName())
                .email(username)
                .emailVerified(true)
                .claim("authorities", tempt.getAuthorities())
                .build()
                .getClaims();
    }
}
