package com.epam.esm.services;

import com.epam.esm.repositories.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ClientService implements RegisteredClientRepository {
    private final ClientRepository clientRepository;

    @Override
    @Transactional
    public void save(RegisteredClient registeredClient) {
        clientRepository.save(Converter.from(registeredClient));
    }

    @Override
    public RegisteredClient findById(String id) {
        return Converter.from(
                clientRepository.findById(Long.parseLong(id))
                    .orElseThrow()
        );
    }

    @Override
    public RegisteredClient findByClientId(String clientId) {
        return Converter.from(
                clientRepository.findAllByClientId(clientId)
                        .orElseThrow()
        );
    }
}
