package com.epam.esm.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Builder.Default
    @ManyToMany(
            mappedBy = "users",
            cascade = {CascadeType.MERGE, CascadeType.PERSIST}
    )
    private Set<Authority> authorities = new HashSet<>();

    public void addAuthority(Authority authority){
        authority.getUsers().add(this);
        authorities.add(authority);
    }
}
