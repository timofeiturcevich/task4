package com.epam.esm.controller;

import com.epam.esm.dto.CodeDTO;
import com.epam.esm.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class AuthController {
    private final UserService userService;

    @GetMapping("/code")
    public ResponseEntity<Object> code(@RequestParam(name = "code") String code){
        return ResponseEntity
                .ok(new CodeDTO(code));
    }

    @PostMapping("/register")
    public ResponseEntity<Long> register(@RequestBody Map<Object,Object> fields){
        return ResponseEntity
                .ok(userService.createOidcUser(fields));
    }

    @PostMapping("/update")
    public ResponseEntity<Long> update(@RequestBody Map<Object,Object> fields){
        return ResponseEntity
                .ok(userService.updateUser(fields));
    }



//    String token = req.getHeader("Authorization").replaceAll("Bearer ","");
//    DecodedJWT decodedJWT = JWT.decode(token);
//        return ResponseEntity
//                .ok(decodedJWT.getClaim("sub").asString());

}
