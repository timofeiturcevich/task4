package com.epam.esm.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CodeDTO {

    private String code;

    private String tokenLink;

    public CodeDTO(String code){
        this.code = code;
        this.tokenLink = "http://localhost:9090/oauth2/token?client_id=task4_client" +
                "&redirect_uri=http://localhost:9090/code" +
                "&grant_type=authorization_code" +
                "&code=" + code +
                "&code_verifier=qPsH306-ZDDaOE8DFzVn05TkN3ZZoVmI_6x4LsVglQI";
    }
}
