package com.epam.esm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserCreatedDTO {
    private String email;

    @Builder.Default
    private String linkToGetToken =
            "http://localhost:9090/oauth2/authorize?response_type=code&client_id=client&scope=openid" +
                    "&redirect_uri=http://localhost:9090/code" +
                    "&code_challenge=QYPAZ5NU8yvtlQ9erXrUYR-T5AGCjCF47vN-KsaI2A8&code_challenge_method=S256";


}
