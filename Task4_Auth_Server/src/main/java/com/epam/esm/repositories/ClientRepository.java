package com.epam.esm.repositories;

import com.epam.esm.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client,Long> {

    @Query("SELECT c FROM Client c WHERE c.clientId = ?1")
    Optional<Client> findAllByClientId(String clientId);
}
