package com.epam.esm.repositories;

import com.epam.esm.entities.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<Authority,Long> {

    @Query("SELECT a FROM Authority a WHERE a.name = ?1")
    Optional<Authority> findAllByName(String name);
}
