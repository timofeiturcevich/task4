package com.epam.esm.controller;

import com.epam.esm.entities.GiftCertificate;
import com.epam.esm.entities.Tag;
import com.epam.esm.entities.User;
import com.epam.esm.entities.UserCertificate;
import com.epam.esm.service.GiftCertificateService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.swing.plaf.synth.SynthUI;
import java.util.Map;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/certificates")
public class CertificateController {
    private final GiftCertificateService giftCertificateService;

    @PostMapping
    @PostAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<GiftCertificate> createCertificate(@RequestBody GiftCertificate giftCertificate) {
        giftCertificateService.createCertificate(giftCertificate);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(giftCertificate);
    }

    @GetMapping
    public Page<GiftCertificate> getAllCertificates(Pageable pageable) {
        return setLinks(giftCertificateService.getCertificates(pageable));
    }

    @DeleteMapping("/delete")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public Page<GiftCertificate> deleteCertificate(@RequestParam Long id, Pageable pageable) {
        return setLinks(giftCertificateService.deleteCertificate(id,pageable));
    }

    //ResponseEntity<List<GiftCertificate>>
    @GetMapping("/containsList")
    public Page<GiftCertificate> getAllCertificatesWithTags(@RequestBody Set<Tag> tags, Pageable pageable) {
        return setLinks(giftCertificateService.getAllCertificatesContainingList(tags,pageable));
    }

    @PatchMapping("/update")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<GiftCertificate> updateCertificate(@RequestBody Map<Object, Object> fields) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(giftCertificateService.updateCertificate(fields));
    }

    @DeleteMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<GiftCertificate> deleteTagFromCertificate(@RequestBody Tag tag, @RequestParam Long certificateId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(giftCertificateService.removeTag(tag, certificateId));
    }

    @GetMapping("/getById")
    public ResponseEntity<GiftCertificate> getCertificateById(@RequestParam Long id) {
        GiftCertificate giftCertificate = giftCertificateService.getCertificate(id);
        setLinksForTags(giftCertificate.getTags());
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(giftCertificate);
    }

    public void setLinksForTags(Set<Tag> tags){
        tags.forEach(i->{
            if(i.getLinks().isEmpty()){
                Link selfLink = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(TagController.class)
                        .getByName(i.getName())).withSelfRel();
                i.add(selfLink);
            }
        });
    }

    public Page<GiftCertificate> setLinks(Page<GiftCertificate> page){
        page.getContent().forEach(i->{
            Link selfCertificateLink = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(CertificateController.class)
                    .getCertificateById(i.getId())).withSelfRel();
            i.add(selfCertificateLink);
            setLinksForTags(i.getTags());
        });
        return page;
    }
}
