package com.epam.esm.controller;

import com.epam.esm.dto.UserCertificateDTO;
import com.epam.esm.entities.User;
import com.epam.esm.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    @GetMapping("/username")
    @PreAuthorize("hasAuthority('ADMIN') or ((hasAuthority('USER') and #a.name.equals(#email)))")
    public ResponseEntity<User> getUserByEmail(@RequestBody String email, Authentication a){
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userService.getByEmail(email));
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public Page<User> getAllUsers(Pageable pageable){
        return userService.getAllUsers(pageable);
    }

    @PostMapping("/buyCertificate")
    @PreAuthorize("hasAuthority('ADMIN') or ((hasAuthority('USER') and #a.name.equals(#userCertificateDTO.email)))")
    public ResponseEntity<User> buyCertificate(@RequestBody UserCertificateDTO userCertificateDTO, Authentication a){
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userService.buyCertificate(userCertificateDTO.getEmail(), userCertificateDTO.getCertificateId()));
    }

    @PatchMapping
    @PreAuthorize("hasAuthority('ADMIN') or ((hasAuthority('USER') and #a.name.equals(#fields.get('email'))))")
    public ResponseEntity<User> updateUser(@RequestBody Map<Object,Object> fields, Authentication a){
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userService.updateUser(fields));
    }

    @DeleteMapping("/delete")
    @PreAuthorize("hasAuthority('ADMIN') or ((hasAuthority('USER') and #a.name.equals(#email)))")
    public Page<User> deleteUser(@RequestParam String email, Pageable pageable, Authentication a){
        return userService.deleteUser(email,pageable);
    }
}
