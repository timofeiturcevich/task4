package com.epam.esm.controller;

import com.epam.esm.dto.UserCertificateDTO;
import com.epam.esm.entities.UserCertificate;
import com.epam.esm.service.BoughtCertificateService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/boughtCertificates")
public class BoughtCertificateController {
    private final BoughtCertificateService boughtCertificateService;

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN') or ((hasAuthority('USER') and #a.name.equals(#email)))")
    public Page<UserCertificate> getBoughtCertificates(@RequestParam String email, Pageable pageable, Authentication a){
        return boughtCertificateService.getBoughtCertificates(email, pageable);
    }

    @DeleteMapping("/cancel")
    @PreAuthorize("hasAuthority('ADMIN') or ((hasAuthority('USER') and #a.name.equals(#userCertificateDTO.email)))")
    public Page<UserCertificate> cancel(@RequestBody UserCertificateDTO userCertificateDTO, Pageable pageable, Authentication a){
        return boughtCertificateService.cancel(userCertificateDTO.getEmail(), userCertificateDTO.getCertificateId(), pageable);
    }
}
