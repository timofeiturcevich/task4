package com.epam.esm.controller;

import com.epam.esm.entities.Tag;
import com.epam.esm.service.TagService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/tags")
public class TagController {
    private final TagService tagService;

    @GetMapping
    public Page<Tag> getAllTags(Pageable pageable) {
        return setLinks(pageable);
    }

    @GetMapping("/all")
    public ResponseEntity<Page<Tag>> getAllTags(){
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(setLinks(Pageable.unpaged()));
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<Tag> createTag(@RequestBody Tag tag) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(tagService.createTag(tag));
    }

    @GetMapping("/name")
    public ResponseEntity<Tag> getByName(@RequestParam String name) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(tagService.getTagByName(name));
    }

    @DeleteMapping("/delete")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public Page<Tag> deleteTag(@RequestParam Long id, Pageable pageable) {
        tagService.deleteTag(id);
        return setLinks(pageable);
    }

    private Page<Tag> setLinks(Pageable pageable) {
        Page<Tag> page = tagService.getAllTags(pageable);
        page.getContent().forEach(i -> {
            Link selfLink = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(TagController.class)
                    .getByName(i.getName())).withSelfRel();
            i.add(selfLink);
        });
        return page;
    }
}
