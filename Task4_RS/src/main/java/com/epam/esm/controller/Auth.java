package com.epam.esm.controller;

import com.epam.esm.dto.AuthDTO;
import com.epam.esm.entities.User;
import com.epam.esm.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class Auth {
    private final UserService userService;

    @GetMapping("/authCodeVerifiers")
    public ResponseEntity<AuthDTO> auth() throws NoSuchAlgorithmException {
        return ResponseEntity
                .ok(new AuthDTO());
    }

    @PostMapping("/register")
    public ResponseEntity<User> register(@RequestBody Map<Object,Object> fields){
        return ResponseEntity
                .ok(userService.createUser(fields));
    }

//    @GetMapping("/test")
//    @PreAuthorize("hasAuthority('admin')")
//    public ResponseEntity<String> test(){
//        return ResponseEntity
//                .ok("nice");
//    }
}
