package com.epam.esm.repositories;

import com.epam.esm.entities.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface TagRepository extends JpaRepository<Tag, Long> {
//    @Query("SELECT t FROM Tag t WHERE t.name IN :tags")
    Set<Tag> findAllByNameIn(Set<String> tags);

    Optional<Tag> findAllByNameEquals(String name);
}
