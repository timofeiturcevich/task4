package com.epam.esm.repositories;

import com.epam.esm.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {
    @Query("SELECT u FROM User u WHERE u.email = ?1")
    Optional<User> findByEmail(String email);

    @Query("DELETE FROM User u WHERE u.email = ?1")
    void deleteByEmail(String email);
}
