package com.epam.esm.repositories;

import com.epam.esm.entities.GiftCertificate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.Set;

public interface CertificateRepository extends JpaRepository<GiftCertificate, Long> {
    //    @QueryHints(@QueryHint(name = "hibernate.query.passDistinctThrough", value = "false"))
    @EntityGraph(attributePaths = "tags", type = EntityGraph.EntityGraphType.FETCH)
//    @Query(value = "SELECT gc FROM GiftCertificate gc JOIN FETCH gc.tags",
//            countQuery = "SELECT COUNT(gc) FROM GiftCertificate gc")
    Page<GiftCertificate> findAllBy(Pageable pageable);

    @Query("SELECT gc FROM GiftCertificate gc JOIN FETCH gc.tags WHERE gc.id = ?1")
    Optional<GiftCertificate> findByIdAnd(Long id);

//    @Query("""
//            SELECT gc FROM GiftCertificate gc
//            JOIN FETCH gc.tags t
//            WHERE t.id IN ?1
//            GROUP BY gc
//            HAVING COUNT(t.id) = ?2
//            """)
//    List<GiftCertificate> findAllByTagsContains(Set<Long> ids, int size);

    @Query(value = """
            SELECT gc FROM GiftCertificate gc
            JOIN gc.tags t
            WHERE t IN ?1
            GROUP BY gc
            HAVING COUNT(DISTINCT t) = ?2
            """,
            countQuery = """
                    SELECT count(gc) FROM GiftCertificate gc
                    JOIN gc.tags t
                    WHERE t IN ?1
                    GROUP BY gc
                    HAVING COUNT(DISTINCT t) = ?2
                            """
    )
    Page<GiftCertificate> findAllByTagsContains(Set<Long> ids, int size, Pageable pageable);

}

