package com.epam.esm.repositories;

import com.epam.esm.entities.UserCertificate;
import com.epam.esm.entities.UserCertificateId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserCertificateRepository extends JpaRepository<UserCertificate, UserCertificateId> {


    Page<UserCertificate> getUserCertificateByUser_Email(String email, Pageable pageable);
}
