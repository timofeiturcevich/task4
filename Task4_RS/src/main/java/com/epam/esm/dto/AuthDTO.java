package com.epam.esm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

@Data
public class AuthDTO {

    private String codeChallenge;

    private String codeVerifier;

    public AuthDTO() throws NoSuchAlgorithmException {
        SecureRandom secureRandom = new SecureRandom();
        byte[] code = new byte[32];
        secureRandom.nextBytes(code);

        this.codeVerifier = Base64.getUrlEncoder()
                .withoutPadding()
                .encodeToString(code);

        byte[] bytes = codeVerifier.getBytes(StandardCharsets.US_ASCII);
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(bytes, 0, bytes.length);

        byte[] digest = messageDigest.digest();
        this.codeChallenge =  Base64.getUrlEncoder()
                .withoutPadding()
                .encodeToString(digest);
    }
}
