package com.epam.esm.dto;

import lombok.Data;

@Data
public class UserCertificateDTO {
    private Long certificateId;

    private String email;
}
