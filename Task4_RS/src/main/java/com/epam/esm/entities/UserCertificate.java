package com.epam.esm.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@Table(name = "user_order")
public class UserCertificate {
    @EqualsAndHashCode.Exclude
    @EmbeddedId
    @JsonIgnore
    private UserCertificateId userCertificateId;

    @ManyToOne
    @JsonBackReference
    @MapsId("userId")
    private User user;

    @ManyToOne
    @MapsId("certificateId")
    private GiftCertificate giftCertificate;

    @Column(name = "price")
    private int price;

    @Column(name = "buy_date")
    private String buyDate = Timestamp.valueOf(LocalDateTime.now()).toString();

    public UserCertificate(User user, GiftCertificate giftCertificate, int price){
        this.price = price;
        this.userCertificateId = new UserCertificateId(user.getId(),giftCertificate.getId());
        this.user = user;
        this.giftCertificate = giftCertificate;
    }
}
