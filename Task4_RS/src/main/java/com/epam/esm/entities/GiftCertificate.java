package com.epam.esm.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "gift_certificate")
public class GiftCertificate extends RepresentationModel<GiftCertificate> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Exclude
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private int price;

    @Column(name = "duration")
    private int duration;

    @Column(name = "create_date")
    private String createDate;

    @Column(name = "last_update_date")
    private String lastUpdateDate;

    //    @JsonManagedReference
//    @Setter(AccessLevel.PRIVATE)
    @ManyToMany(mappedBy = "giftCertificates", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Tag> tags = new HashSet<>();

    @JsonIgnore
    @Setter(AccessLevel.PRIVATE)
    @OneToMany(
            mappedBy = "giftCertificate",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE}
    )
    private List<UserCertificate> users = new ArrayList<>();

    public GiftCertificate(Long id, String name, String description, int price, int duration, String createDate, String lastUpdateDate, Set<Tag> tags){
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.duration = duration;
        this.createDate = createDate;
        this.lastUpdateDate = lastUpdateDate;
        this.tags = tags;
    }

    public void addTag(Tag tag) {
        tag.getGiftCertificates().add(this);
        tags.add(tag);
    }

    public void addAllTags(Set<Tag> tags) {
        for (Tag tempt : tags) {
            addTag(tempt);
        }
    }

    public void removeTag(Tag tag){
        tag.getGiftCertificates().remove(this);
        tags.remove(tag);
    }

    public void removeAllTags(Set<Tag> tags){
        for(Tag tempt : tags){
            removeTag(tempt);
        }
    }
}
