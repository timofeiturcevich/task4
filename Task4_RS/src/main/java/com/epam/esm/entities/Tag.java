package com.epam.esm.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.NaturalId;
import org.springframework.hateoas.RepresentationModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "tag")
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Tag extends RepresentationModel<Tag> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", unique = true)
    private String name;


    @Setter(AccessLevel.PRIVATE)
    @JsonIgnore
    @ManyToMany
    @ToString.Exclude
    @JoinTable(name = "certificate_tag",
            joinColumns = @JoinColumn(name = "tag_id"),
            inverseJoinColumns = @JoinColumn(name = "certificate_id")
    )
    private List<GiftCertificate> giftCertificates = new ArrayList<>();

    public Tag(Long id, String name){
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tag tag = (Tag) o;
        if(Objects.equals(this.id, tag.id)){
            return true;
        }
        return Objects.equals(this.name, tag.name);
    }

    @Override
    public int hashCode() {
        return 17;
    }
}
