package com.epam.esm.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "email")
    private String email;

    @JsonIgnore
    @Builder.Default
    @Setter(AccessLevel.PRIVATE)
    @OneToMany(
            mappedBy = "user",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE}
    )
    private List<UserCertificate> certificates = new ArrayList<>();

    @JsonManagedReference
    public List<UserCertificate> getCertificates() {
        return certificates;
    }

    public void addCertificate(GiftCertificate giftCertificate){
        UserCertificate userCertificate = new UserCertificate(this,giftCertificate,giftCertificate.getPrice());
        certificates.add(userCertificate);
        giftCertificate.getUsers().add(userCertificate);
    }
}
