package com.epam.esm.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Data;

import java.io.Serializable;

@Embeddable
@Data
public class UserCertificateId implements Serializable {
    @Column(name = "user_id")
    private Long userId;

    @Column(name= "gift_certificate_id")
    private Long certificateId;

    public UserCertificateId(){}
    public UserCertificateId(Long userId, Long certificateId){
        this.userId = userId;
        this.certificateId = certificateId;
    }

}
