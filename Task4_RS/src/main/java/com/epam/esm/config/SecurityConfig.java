package com.epam.esm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableMethodSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.oauth2ResourceServer(
                i-> i.jwt(
                        j->j.jwkSetUri("http://localhost:9090/oauth2/jwks")
                                .jwtAuthenticationConverter(new CustomJwtAuthenticationConverter())
                )
        );

        http.csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(
                        i-> i.requestMatchers("/authCodeVerifiers","/register","/certificates","/tags","/tags/all").permitAll()
                                .anyRequest().authenticated()
                );


        return http.build();
    }

}
