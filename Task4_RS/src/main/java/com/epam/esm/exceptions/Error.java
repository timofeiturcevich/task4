package com.epam.esm.exceptions;


import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Error {
    private int errorCode;

    private String errorMessage;
}
