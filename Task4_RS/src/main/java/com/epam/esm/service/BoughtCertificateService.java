package com.epam.esm.service;

import com.epam.esm.entities.UserCertificate;
import com.epam.esm.entities.UserCertificateId;
import com.epam.esm.exceptions.EntityNotFoundException;
import com.epam.esm.repositories.UserCertificateRepository;
import com.epam.esm.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class BoughtCertificateService {
    private final UserCertificateRepository userCertificateRepository;
    private final UserRepository userRepository;

    @Transactional
    public Page<UserCertificate> getBoughtCertificates(String email, Pageable pageable){
        return userCertificateRepository.getUserCertificateByUser_Email(email,pageable);
    }

    @Transactional
    public Page<UserCertificate> cancel(String email, Long certificateId, Pageable pageable){
        userCertificateRepository.deleteById(new UserCertificateId(
                userRepository.findByEmail(email)
                        .orElseThrow(()->new EntityNotFoundException("Something went wrong during operation. User email might be wrong. If you dont enter eny data, try to login again, or repeat operation later"))
                        .getId()
                ,certificateId)
        );
        return userCertificateRepository.getUserCertificateByUser_Email(email, pageable);
    }

}
