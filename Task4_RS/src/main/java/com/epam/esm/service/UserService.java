package com.epam.esm.service;

import com.epam.esm.entities.GiftCertificate;
import com.epam.esm.entities.User;
import com.epam.esm.exceptions.EntityNotFoundException;
import com.epam.esm.exceptions.RegistrationFailedException;
import com.epam.esm.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Field;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final GiftCertificateService giftCertificateService;

    @Transactional
    public User createUser(Map<Object,Object> fields){
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Map<Object,Object>> req = new HttpEntity<>(fields,new HttpHeaders());
        ResponseEntity<Long> response = restTemplate.postForEntity("http://localhost:9090/register", req, Long.class);

        if(response.getStatusCode()!=HttpStatus.OK){
            throw new RegistrationFailedException("Something went wrong. Check fields you entered, ot try again later");
        }

        User user = User.builder()
                .email(fields.get("email").toString())
                .build();

        return userRepository.save(user);
    }

    public Page<User> getAllUsers(Pageable pageable){
        return userRepository.findAll(pageable);
    }

    @Transactional
    public User getById(Long id){
        return userRepository.findById(id)
                .orElseThrow(()->new EntityNotFoundException("Such user doesn't found"));
    }

    @Transactional
    public User getByEmail(String email){
        return userRepository.findByEmail(email)
                .orElseThrow(()->new EntityNotFoundException("Something went wrong during operation. User email might be wrong. If you dont enter eny data, try to login again, or repeat operation later"));
    }

    @Transactional
    public User buyCertificate(String email, Long certificateId){
        User user = userRepository.findByEmail(email)
                .orElseThrow(()->new EntityNotFoundException("Something went wrong during operation. User email might be wrong. If you dont enter eny data, try to login again, or repeat operation later"));
        GiftCertificate giftCertificate = giftCertificateService.getCertificate(certificateId);
        user.addCertificate(giftCertificate);
        return user;
    }

    @Transactional
    public User updateUser(Map<Object,Object> fields){
        User user = userRepository.findByEmail((String) fields.get("email"))
                .orElseThrow(()->new EntityNotFoundException("Something went wrong during operation. User email might be wrong. If you dont enter eny data, try to login again, or repeat operation later"));

        if(fields.containsKey("password")){
            RestTemplate restTemplate = new RestTemplate();
            HttpEntity<Map<Object,Object>> req = new HttpEntity<>(fields,new HttpHeaders());
            ResponseEntity<Long> response = restTemplate.postForEntity("http://localhost:9090/update", req, Long.class);

            if(response.getStatusCode()!=HttpStatus.OK){
                throw new RuntimeException();
            }
        }

        fields.remove("id");
        fields.remove("password");
        fields.remove("email");
        fields.forEach((key,value)->{
            Field field = ReflectionUtils.findField(User.class,(String) key);
            field.setAccessible(true);
            ReflectionUtils.setField(field,user,value);
        });
        return user;
    }

    public Page<User> deleteUser(String email, Pageable pageable){
        userRepository.deleteByEmail(email);
        return userRepository.findAll(pageable);
    }
}
