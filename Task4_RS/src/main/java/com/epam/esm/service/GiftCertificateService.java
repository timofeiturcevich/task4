package com.epam.esm.service;

import com.epam.esm.entities.GiftCertificate;
import com.epam.esm.entities.Tag;
import com.epam.esm.entities.UserCertificate;
import com.epam.esm.exceptions.EntityNotFoundException;
import com.epam.esm.repositories.CertificateRepository;
import com.epam.esm.repositories.UserCertificateRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GiftCertificateService {
    private final CertificateRepository certificateRepository;
    private final TagService tagService;

    public Page<GiftCertificate> getCertificates(Pageable pageable){
        return certificateRepository.findAllBy(pageable);
    }

    @Transactional
    public GiftCertificate getCertificate(Long id){
        return certificateRepository.findByIdAnd(id)
                .orElseThrow(()->new EntityNotFoundException("Something went wrong during operation. Gift certificate wasn't found"));
    }

    public Page<GiftCertificate> getAllCertificatesContainingList(Set<Tag> tags, Pageable pageable){
        tags = tagService.parseCertificateTags(tags);
        return certificateRepository.findAllByTagsContains(tags.stream().map(Tag::getId).collect(Collectors.toSet()), tags.size(), pageable);
    }

    @Transactional
    public void createCertificate(GiftCertificate giftCertificate){
        giftCertificate.setCreateDate(Timestamp.valueOf(LocalDateTime.now()).toString());
        giftCertificate.setLastUpdateDate(Timestamp.valueOf(LocalDateTime.now()).toString());


        System.out.println("test");

        Set<Tag> tempt = tagService.parseCertificateTags(giftCertificate.getTags());
        giftCertificate.getTags().clear();
        giftCertificate.addAllTags(tempt);


        certificateRepository.save(giftCertificate);
    }

    public Page<GiftCertificate> deleteCertificate(Long id, Pageable pageable){
        certificateRepository.deleteById(id);
        return certificateRepository.findAll(pageable);
    }

    @Transactional
    public GiftCertificate updateCertificate(Map<Object,Object> fields){
        GiftCertificate giftCertificate = certificateRepository.findByIdAnd(Long.parseLong(fields.get("id").toString()))
                .orElseThrow(()->new EntityNotFoundException("Something went wrong during operation. Gift certificate wasn't found. Check if you entered wrong id, or try again later"));

        if (!((ArrayList<LinkedHashMap<Object,Object>>)fields.get("removeTags")).isEmpty()) {
            parseFieldsTags(fields,"removeTags");
            giftCertificate.removeAllTags((HashSet<Tag>)fields.get("removeTags"));
        }

        if(!((ArrayList<LinkedHashMap<Object,Object>>)fields.get("tags")).isEmpty()){
            parseFieldsTags(fields,"tags");
            giftCertificate.addAllTags((HashSet<Tag>)fields.get("tags"));
        }

        fields.remove("tags");
        fields.remove("removeTags");
        fields.remove("id");

        fields.forEach((key,value)->{
            Field field = ReflectionUtils.findField(GiftCertificate.class,(String) key);
            field.setAccessible(true);
            ReflectionUtils.setField(field,giftCertificate,value);
        });
        Timestamp tempt = Timestamp.valueOf(LocalDateTime.now());
        tempt.setNanos(0);
        giftCertificate.setLastUpdateDate(tempt.toString().substring(0,tempt.toString().length()-2));

        return giftCertificate;
    }

    public void parseFieldsTags(Map<Object, Object> fields, String tagType){
        fields.put(tagType,((ArrayList<LinkedHashMap<Object,Object>>) fields.get(tagType)).stream().map(i->{
            Tag tempt = new Tag();
            tempt.setId(Long.valueOf((Integer) i.get("id")));
            tempt.setName((String) i.get("name"));
            return tempt;
        }).collect(Collectors.toSet()));
        fields.put(tagType,tagService.parseCertificateTags((HashSet<Tag>)fields.get(tagType)));
    }

    @Transactional
    public GiftCertificate removeTag(Tag tag, Long id){
        GiftCertificate giftCertificate = certificateRepository.findById(id)
                .orElseThrow();
        giftCertificate.getTags().remove(tag);
        return giftCertificate;
    }
}
