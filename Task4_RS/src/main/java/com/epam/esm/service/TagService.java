package com.epam.esm.service;

import com.epam.esm.entities.Tag;
import com.epam.esm.exceptions.EntityNotFoundException;
import com.epam.esm.repositories.TagRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TagService {
    private final TagRepository tagRepository;

    public Page<Tag> getAllTags(Pageable pageable){
        return tagRepository.findAll(pageable);
    }

    public Tag createTag(Tag tag){
        return tagRepository.save(tag);
    }

    public Tag getTagByName(String name){
        return tagRepository.findAllByNameEquals(name)
                .orElseThrow(()->new EntityNotFoundException("Something went wrong during operation. Tag with such name("+ name + ") wasn't found"));
    }

    @Transactional
    public Set<Tag> parseCertificateTags(Set<Tag> tags){
        Set<Tag> tempt = new HashSet<>(tags);

        Set<Tag> foundTags = new HashSet<>(tagRepository.findAllById(tempt.stream().map(Tag::getId).filter(Objects::nonNull).collect(Collectors.toSet())));
        tempt.removeAll(foundTags);

        foundTags.addAll(tagRepository.findAllByNameIn(tempt.stream().map(Tag::getName).filter(Objects::nonNull).collect(Collectors.toSet())));
        tempt.removeAll(foundTags);
        foundTags.addAll(tagRepository.saveAll(tempt.stream().filter(i->i.getName()!=null).collect(Collectors.toSet())));

//        tempt.forEach(i->{
//            Tag temptTag = tagRepository.findAllByNameEquals(i.getName())
//                    .orElse(tagRepository.save(i));
//            foundTags.add(temptTag);
//        });


        return foundTags;
    }

    public void deleteTag(Long id){
        tagRepository.deleteById(id);
    }
}
