package com.epam.esm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task4RsApplication {

    public static void main(String[] args) {
        SpringApplication.run(Task4RsApplication.class, args);
    }

}
